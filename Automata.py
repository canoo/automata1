#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Estado:
    """
    Clase que representa un estado de un automata finito.
    La clase estado tiene los siguientes atributos:

    -nombre
    -transiciones, que se representan por un mapa de simbolos y
     estados
    -es_final, un booleano que indica si el estado es final
    -alfabeto, el alfabeto total

    La clase estado tiene los siguientes metodos:
    -hallar el hash
    -comprobar la igualdad (se supone que dos objetos con el mismo
     nombre son iguales)
    -anadir letra
    -anadir transicion
    -leer letra
    -escribir el objeto por pantalla
    -devolver el nombre del estado, el alfabeto y si el estado es final
    """

    def __init__(self, nombre, es_final):
        """
        Constructor al que se le pasa el nombre y si es final
        """
        self.nombre = nombre
        self.transiciones = {}
        self.es_final = es_final
        self.alfabeto = set([])

    def __hash__(self):
        """
        Retorna el valor hash de un estado
        """
        return hash(self.nombre)

    def __eq__(self,otro_estado):
        """
        Retorna si dos estados son iguales
        Argumento:
        - `otro_estado`:El estado para comprar si son iguales
        """
        return self.nombre == otro_estado.nombre

    def __str__(self):
        """
        Metodo para hacer la conversion a string
        """
        resultado=''
        resultado += self.nombre + '\n'
        resultado += 'estado final: ' + str(self.es_final) + '\n'
        for l, lista in self.transiciones.items():
            resultado += str(l) + ' : '
            for e in lista:
                resultado+= e.ver_nombre()+ ','
            resultado+='\n'
        return resultado

    def pon_letra(self, letras):
        """
        Metodo que anade letras al alfabeto
        """
        if isinstance(letras, list):
            for i in letras:
                if i not in self.alfabeto:
                    self.alfabeto.add(str(i))
        else:
            if i not in self.alfabeto:
                self.alfabeto.add(str(letras))

    def pon_transicion(self, diccionario_nuevas_transiciones):
        """
        Metodo para anadir a nuestras transiciones.
        Si para una letra, hay mas de un estado entonces poner una
        lista de estados como valor del simbolo
        """
        for key in diccionario_nuevas_transiciones:
            if key in self.transiciones.keys():
                self.transiciones[key].append(diccionario_nuevas_transiciones[key])
            else:
                self.transiciones[key]=diccionario_nuevas_transiciones[key]

    def lee_letra(self, letra):
        """
        Metodo que devuelve los estados a los que se va despues de
        leer una letra
        """
        return set(self.transiciones[letra])

    def ver_nombre(self):
        """
        Metodo observador del nombre
        """
        return self.nombre

    def ver_alfabeto(self):
        """
        Metodo observador del alfabeto
        """
        return self.alfabeto

    def ver_es_final(self):
        """
        Metodo observador del alfabeto
        """
        return self.es_final




class AutomataFinito:
    """
    Clase que representa un automata finito.
    La clase estado tiene dos atributos:

    -estados: el conjunto de estados
    -inicial: el estado inicial
    -alfabeto: el conjunto de letras que es la union de todos los alfabetos de
     los estados
    """

    def __init__(self, estado_inicial ):
        """
        Constructor al que se le pasa el estado inicial
        """
        self.estados=set([])
        self.inicial = estado_inicial
        self.alfabeto = set([])
    def __str__(self):
        """
        Metodo para representar un automata
        """
        resultado = "Inicial :" + str(self.inicial)
        resultado +=" \nEstados: \n"
        for estado in self.estados:
            resultado += str(estado)
        return resultado

    def poner_alfabeto_total(self):
        """
        Metodo que pone el mismo alfabeto a todos los estados. Es
        conveniente llamarlo despues de anadir un estado
        """
        for i in self.estados:
            self.alfabeto  = self.alfabeto | i.alfabeto
        for i in self.estados:
            i.alfabeto = self.alfabeto

    def anade_estado(self,otro_estado):
        """
        Metodo que anade un estado al conjunto de estados
        Argumento:
        - `otro_estado`: estado a anadir al conjunto de estados
        """
        self.estados.add(otro_estado)
        self.poner_alfabeto_total()

    def completar(self):
        """
        Metodo que completa el automata, poniendo un nuevo estado de
        error y anadiendo transiciones a ese nuevo estado para cada
        letra del alfabeto y para cada estado que no tenga definida
        esa letra
        """
        self.error = Estado('Error', False)
        for i in self.estados:
            faltan = self.alfabeto - i.transiciones.keys()
            i.pon_transicion({ l:self.error for l in faltan})

    def es_aceptada(self,palabra):
        """
        Comprueba si una palabra es aceptada por el automata
        Argumento:
        - `palabra`: palabra a leer por el automata, la palabra
        deberia ser un objeto iterable (se recomienda una lista o un string)
        """

        finales = self.calculo(palabra,self.inicial)
        for estado in finales:
            if estado.es_final:
                return True
        return False


    def calculo(self, palabra, estado_inicio):
        """
        Metodo que calcula a que estados se llegan despues de leer una palabra
        Argumentos:
        - `estado_inicio`: Estado desde el que se comienza a leer
        - `palabra`: Palabra que se quiere leer
        """
        estadosSiguientes= {estado_inicio}
        for letra in palabra:
            estados = set([])
            for e in estadosSiguientes:
                sig = e.lee_letra(letra)
                """print("Estado "+ str(e)+ "letra: "+ letra)
                for ee in es:
                    print(ee)"""
                estados = estados | sig
            #print(len(estados))
            estadosSiguientes = estados

        return estadosSiguientes

    def determinizar(self):
        """
        Metodo que devuelve un automata determinista que acepta el
        mismo lenguaje que self.
        """
        pass


if __name__ == '__main__':
    #Este metodo main es una forma para testar los programas que se
    #han hecho. Quiza sea conveniente que el alumno modifique los
    #datos de entrada.
        q0 = Estado('nombre0',False)
        q0.pon_letra(["1","0"])
        q1 = Estado('nombre1',False)
        q2 = Estado('nombre2',False)
        q3 = Estado('nombre3',True)
        q0.pon_transicion({'0': [q0,q1]})
        q0.pon_transicion({'1': [q0]})
        q1.pon_transicion({'0': [q2]})
        q1.pon_transicion({'1': [q2]})
        q2.pon_transicion({'0': [q3]})
        q2.pon_transicion({'1': [q3]})
        A = AutomataFinito(q0)
        A.anade_estado(q1)
        A.anade_estado(q2)
        A.anade_estado(q3)
        B = A
        B.estados.add(B.inicial)
        B.completar()

        for i in q2.transiciones.keys():
            print(str(i))
            print(str(q2.transiciones[i]))
