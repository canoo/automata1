#! /opt/local/bin/python3
# -*- coding: utf-8 -*-
__autor__="Alvaro, Diego, Álvar"
izip=zip
import unicodedata
import re
import unittest
import inspect
from itertools import product

class Correccion(unittest.TestCase):

    comments = open("comentario0.txt",'w')
    mark = open("notas0.txt",'w')

    def escribir_nota_comment(self, comment, kwargs = dict()):
        self.comments.write("\n<br>"+comment+"<br>\n")
        for key, value in kwargs.iteritems():
            self.comments.write(str(key)+':<br>\n')
            self.comments.write(str(value)+'<br>\n')
            if key == 'mark':
                self.mark.write(value+str('\n'))

    def test_calculo_sin_deter_5(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('nombre0',False)
            q1 = Estado('nombre1',False)
            q2 = Estado('nombre2',False)
            q3 = Estado('nombre3',True)
            q0.pon_transicion({'0': [q0,q1]})
            q0.pon_transicion({'1': [q0]})
            q1.pon_transicion({'0': [q2]})
            q1.pon_transicion({'1': [q2]})
            q2.pon_transicion({'0': [q3]})
            q2.pon_transicion({'1': [q3]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            A.anade_estado(q3)
            B = A
            B.estados.add(B.inicial)
            if len(B.estados) in (6,7,8):
                notas = {'mark': '5'}
                self.escribir_nota_comment('El numero de estados esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('El numero de estados falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('El numero de estados lanza:\
            <br>'+str(e)+'<br>')


    def test_calculo_sin_deter_4(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('nombre0',False)
            q1 = Estado('nombre1',False)
            q2 = Estado('nombre2',False)
            q3 = Estado('nombre3',True)
            q0.pon_transicion({'0': [q0,q1]})
            q0.pon_transicion({'1': [q0]})
            q1.pon_transicion({'0': [q2]})
            q1.pon_transicion({'1': [q2]})
            q2.pon_transicion({'0': [q3]})
            q2.pon_transicion({'1': [q3]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            A.anade_estado(q3)
            B = A
            bien = True
            for est in B.estados:
                for k,v in est.transiciones.items():
                    if not isinstance(v,Estado) and len(v)>1:
                        bien=False
                        raise Exception("Mira el estado %s con %s"%(str(est),str(k)))

            if bien:
                notas = {'mark': '25'}
                self.escribir_nota_comment('Sin_Deter esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('Sin_Deter falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('Mira la siguiente excepcion:\
            <br>'+str(e)+'<br>')
        return


    def test_es_aceptada_sin_deter3(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('nombre0',False)
            q1 = Estado('nombre1',False)
            q2 = Estado('nombre2',False)
            q3 = Estado('nombre3',True)
            q0.pon_transicion({'0': [q0,q1]})
            q0.pon_transicion({'1': [q0]})
            q1.pon_transicion({'0': [q2]})
            q1.pon_transicion({'1': [q2]})
            q2.pon_transicion({'0': [q3]})
            q2.pon_transicion({'1': [q3]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            A.anade_estado(q3)
            B = A
            bien = True
            for i in product(['0','1'],repeat=5):
                palabra = ''.join(i)
                if B.es_aceptada(palabra) != (palabra[-3] == '0'):
                    raise Exception("Mira la palabra"%(palabra))
            if bien:
                notas = {'mark': '20'}
                self.escribir_nota_comment('Automata.es_aceptada con\
                sin_deter esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('Automata.es_aceptada con\
                sin_deter falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('Automata.es_aceptada lanza:\
            <br>'+str(e)+'<br>')
        return

    def test_calculo_sin_deter_1(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('nombre0',False)
            q1 = Estado('nombre1',False)
            q2 = Estado('nombre2',True)
            q0.pon_transicion({'0': [q0,q1]})
            q1.pon_transicion({'0': [q2]})
            q2.pon_transicion({'1': [q2]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            if len(A.calculo('11', q0)) == 1:
                notas = {'mark': '5'}
                self.escribir_nota_comment('Automata.calculo indeterminista con completar esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('Automata.calculo indeterminista con completar falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('Automata.calculo indeterminista con completar lanza:\
            <br>'+str(e)+'<br>')


    def test_calculo_sin_deter_2(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('q0',False)
            q1 = Estado('q1',False)
            q2 = Estado('q2',True)
            q0.pon_transicion({'0': [q0,q1]})
            q1.pon_transicion({'0': [q2]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            Adet = A
            if len(Adet.calculo('00', Adet.inicial))==1:
                notas = {'mark': '20'}
                self.escribir_nota_comment('Automata.calculo esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('Automata.calculo falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('Automata.calculo lanza:\
            <br>'+str(e)+'<br>')
        return


    def test_es_aceptada_sin_deter(self):
        """

        Arguments:
        - `self`:
        """

        try:
            q0 = Estado('nombre0',False)
            q1 = Estado('nombre1',False)
            q2 = Estado('nombre2',True)
            q0.pon_transicion({'0': [q0, q1]})
            q1.pon_transicion({'0': [q2]})
            A = AutomataFinito(q0)
            A.anade_estado(q1)
            A.anade_estado(q2)
            A.completar()
            if A.es_aceptada('00'):
                notas = {'mark': '20'}
                self.escribir_nota_comment('Automata.es_aceptada esta bien.<br>', notas)
            else:
                self.escribir_nota_comment('Automata.es_aceptada falla.<br>')
        except Exception as e:
            self.escribir_nota_comment('Automata.es_aceptada lanza:\
            <br>'+str(e)+'<br>')
        return




if __name__ == '__main__':
    with open('Automata.py','r') as f:
        t = f.read()
        s = t
    with open('Automata1.py','w') as f:
        f.write(s)
    from Automata import *
    unittest.main()
